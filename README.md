# Extra KiCAD symbols, packages and 3D shapes

## Usage

1. Add this repository to your project as Git submodule
2. Add KiCAD project path *KIEXTLIB*
3. Add required symbol libraries and modules to KiCAD project
